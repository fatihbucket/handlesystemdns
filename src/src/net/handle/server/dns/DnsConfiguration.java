/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.

        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
          http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.handle.server.dns;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import net.handle.apps.simple.SiteInfoConverter;
import net.handle.dnslib.ResourceRecord;
import net.cnri.util.StreamTable;
import net.handle.hdllib.*;
import net.handle.dnslib.DomainName;
import net.handle.dnslib.NameResolver;
import net.handle.dnslib.NameServer;
import net.handle.dnslib.ParseException;
import net.handle.dnslib.Storage;
import net.handle.dnslib.StubResolver;
import net.handle.server.HandleServer;
import net.handle.server.AbstractServer;

public class DnsConfiguration {

	private int thisServerNum = -1;
    private AbstractServer server;
    SiteInfo thisSite = null;
    private Map<String,String> soaRecordMap;
    private Map<String,String> dnsHandleMap;
    private List<InetAddress> dns_forward;
    private boolean recursive = false;
    private InetAddressRange[] allow_recursion;
    private InetAddressRange[] allow_query;
    private int dnsCacheSize = 10000; // currently used for internal cache
    private int dnsCachePrefetcherThreads = 10;
    private boolean certify = false;
    private boolean isPrimary = false;
    
    public DnsConfiguration(AbstractServer server, StreamTable config) throws UnknownHostException {
        if(config==null) config = new StreamTable();
        this.server = server;
        
        try{
        	initThisSiteAndServerNum(config);    
        }
        catch(Exception pe){
        	pe.printStackTrace();
        }
        
        // DNS specific configuration
        
        recursive = config.getBoolean("recursion");
        
        this.certify = config.getBoolean("certify");
        
        Object allow_recursion_raw = config.get("allow_recursion");
        if(allow_recursion_raw instanceof String) allow_recursion = new InetAddressRange[] { new InetAddressRange((String)allow_recursion_raw) };
        else if(allow_recursion_raw != null){
            List allow_recursion_list = (List)allow_recursion_raw;
            allow_recursion = new InetAddressRange[allow_recursion_list.size()];
            for(int i = 0; i<allow_recursion.length; i++) {
                allow_recursion[i] = new InetAddressRange((String)allow_recursion_list.get(i));
            }
        }
        
        Object allow_query_raw = config.get("allow_query");
        if(allow_query_raw instanceof String) allow_query = new InetAddressRange[] { new InetAddressRange((String)allow_query_raw) };
        else if(allow_query_raw != null){
            List allow_query_list = (List)allow_query_raw;
            allow_query = new InetAddressRange[allow_query_list.size()];
            for(int i = 0; i<allow_query.length; i++) {
                allow_query[i] = new InetAddressRange((String)allow_query_list.get(i));
            }
        }        

        String cacheString = (String)config.get("dns_cache_size");
        if(cacheString!=null) dnsCacheSize = Integer.parseInt(cacheString);

        String prefetcherString = (String)config.get("dns_cache_prefetcher_threads");
        if(prefetcherString!=null) dnsCachePrefetcherThreads = Integer.parseInt(prefetcherString);

        Object dns_forward_raw = config.get("dns_forward");
        if(dns_forward_raw instanceof String) dns_forward = Collections.singletonList(InetAddress.getByName((String)dns_forward_raw));
        else if(dns_forward_raw != null){
            List<String> dns_forward_strings = (List)dns_forward_raw;
            dns_forward = new ArrayList<InetAddress>();
            for(String s : dns_forward_strings) {
                dns_forward.add(InetAddress.getByName(s));
            }
        }
        if(dns_forward==null) {
            if(recursive==true || allow_recursion!=null) {
                System.err.println("Warning: DNS recursion support requested, but no dns_forward server configured");
                recursive = false;
                allow_recursion = null;
            }
        }
        else {
            _resolver = new StubResolver(dns_forward, false);
            _resolver.setCache(dnsCachePrefetcherThreads,dnsCacheSize);
        }
        
       
   
        /*the global handleDNSZone*/
        String handleDNSZone = (String)config.get("handle_dns_zone");
       
        /*only for LHS not for GHR */
        String soaRecordHandle =   (String)config.get("soa_record_handle"); 
        /*only for LHS not for GHR */
        String nsRecordHandle =   (String)config.get("ns_record_handle");   
    
        /*only for LHS not for GHR !!!!!*/
        dnsHandleMap = (Map)config.get("dns_handle_map");
        if(dnsHandleMap!=null) {
        	/* for the LHS */
            _storageList = new ArrayList<Storage>(dnsHandleMap.size());
            for(Map.Entry<String,String> entry : dnsHandleMap.entrySet()) {
                try {
                	writeToFile("dns_handle_map and storage FOR="+entry.getKey());       
                	Storage storage = new Storage(DomainName.ofString(entry.getKey()), entry.getValue(), server, this);
                	storage.setHandleZone(DomainName.ofString(handleDNSZone));
      
                	if(isPrimary)
                		storage.soaResourceRecord = storage.resolveSOARecord(soaRecordHandle,DomainName.ofString(entry.getKey()),false);
                	storage.nsResourceRecords = storage.resolveNSRecords(nsRecordHandle,DomainName.ofString(entry.getKey()),false);
                	 _storageList.add(storage);
                    //_storageList.add(new Storage(DomainName.ofString(entry.getKey()), entry.getValue(), server, this));
                }
                catch(ParseException e) {
                    e.printStackTrace();
                }
                catch(HandleException he){
               	 he.printStackTrace();
               }
            }
            _nameServer = new NameServer(storageList(),_resolver,this);
            _nameServer.setCache(dnsCachePrefetcherThreads,dnsCacheSize);
            _nameServer.thisIsLHS = true;
        }
        else{
            try {
            	/* for the GHR, there is no dns_handle_map, instead only handle_dns_zone, without a handlePrefix*/
            	Storage storage = new Storage(DomainName.ofString(handleDNSZone), null, server, this);
            	storage.setHandleZone(DomainName.ofString(handleDNSZone));
            		if(isPrimary) 
            			storage.soaResourceRecord = getSOAforGHR(handleDNSZone,config);
            		storage.nsResourceRecords = getNSforGHR(handleDNSZone,config);
            		
            	
            	
            	_storageList = new ArrayList<Storage>();
        		_storageList.add(storage);
        		_nameServer = new NameServer(storageList(),_resolver,this);
        		_nameServer.setCache(dnsCachePrefetcherThreads,dnsCacheSize);
        		_nameServer.thisIsLHS = false;
            }catch(ParseException e) {
                e.printStackTrace();
           }
        }
    }
    
    
    private void initThisSiteAndServerNum(StreamTable config) throws Exception {
        // get the site info that describes this site that this server is
		// a part of
		try {
			// get the index of this server in the site info
			HandleServer hserver = (HandleServer)server;
		//	writeToFile("DnsConfig:initThisSiteAndServerNum");
			int thisId = hserver.thisServerId;
			//int thisId = Integer.parseInt((String)config.get(hserver.THIS_SERVER_ID));

			// get the site information from the site-info file
			SiteInfo site;
			File siteInfoFile = new File(server.getConfigDir(), HandleServer.SITE_INFO_FILE);
			if(!siteInfoFile.exists() || !siteInfoFile.canRead()) {
			    File siteInfoJsonFile = new File(server.getConfigDir(), HandleServer.SITE_INFO_JSON_FILE);
			    if(!siteInfoJsonFile.exists() || !siteInfoJsonFile.canRead()) {
			        System.err.println("Missing or inaccessible site info file: "+
			                siteInfoFile.getAbsolutePath() + "/" + siteInfoJsonFile.getName());
			        throw new Exception("Missing or inaccessible site info file: "+
			                siteInfoFile.getAbsolutePath() + "/" + siteInfoJsonFile.getName());
			    } else {
			        byte[] siteInfoBytes = Util.getBytesFromFile(siteInfoJsonFile);
			        try {
			            site = SiteInfoConverter.convertToSiteInfo(new String(siteInfoBytes,"UTF-8"));
			        } catch(Throwable t) {
	                    System.err.println("Missing or inaccessible site info file: "+
	                            siteInfoFile.getAbsolutePath() + "/" + siteInfoJsonFile.getName());
	                    throw new Exception("Missing or inaccessible site info file: "+
	                            siteInfoFile.getAbsolutePath() + "/" + siteInfoJsonFile.getName(),t);
			        }
			    }
			}
			else {
			    site = new SiteInfo();
			    byte siteInfoBuf[] = new byte[(int)siteInfoFile.length()];
			    InputStream in = new FileInputStream(siteInfoFile);
			    try {
			    	int r, n = 0;
			    	while ((r = in.read(siteInfoBuf, n, siteInfoBuf.length - n)) > 0) n+= r;
			    } finally {
			    	in.close();
			    }
			    Encoder.decodeSiteInfoRecord(siteInfoBuf, 0, site);
			}

			thisServerNum = -1;
			for(int i=0; i<site.servers.length; i++) {
				if(site.servers[i].serverId==thisId) {
					thisServerNum = i;
				}
			}

			if(thisServerNum<0) {
				throw new Exception("Server ID "+thisId+" does not exist in site!");
			}
			thisSite = site;
		} catch (Exception e) {
			System.err.println("Invalid site/server specification: "+e);
			throw e;
		}
		isPrimary = thisSite.isPrimary;
    }
        
    private ResourceRecord getSOAforGHR(String handleDNSZone,StreamTable config){
    	
    	
        soaRecordMap = (Map)config.get("soa_record");
        String soaTtl = null;
        String soaNS = null;
        String soaEmail = null;
        String soaSn = null;
        String soaRef = null;
        String soaRet = null;
        String soaEx = null;
        String soaNx = null;
        StringBuilder soaDataAsString = new StringBuilder();
        for(Map.Entry<String,String>entry : soaRecordMap.entrySet()){
        	String key = entry.getKey();
        	String value = entry.getValue();
        	if (key.equals("ttl")){
        		 soaTtl = value;
        	}
        	else if (key.equals("ns")){
        		 soaNS = value;
        		 
        	}
        	else if (key.equals("email")){
       
        		 soaEmail = value;
        		 
        	}
          	else if (key.equals("sn")){
        		 soaSn = value;
        		 
          	}
          	else if (key.equals("ref")){

        		 soaRef = value;
        		
          	}
          	else if (key.equals("ret")){
        		 soaRet = value;
        		
          	}
          	else if (key.equals("ex")){

        		 soaEx = value;
        		
          	}
          	else if (key.equals("nx")){

        		 soaNx = value;
        		
          	}
        	
        }
        soaDataAsString.append(soaNS+" ");
        soaDataAsString.append(soaEmail+" ");
        soaDataAsString.append(soaSn+" ");
        soaDataAsString.append(soaRef+" ");
        soaDataAsString.append(soaRet+" ");
        soaDataAsString.append(soaEx+" ");
        soaDataAsString.append(soaNx);
        ResourceRecord soaResourceRecord = null;
        try{
        	writeToFile("DnsConfig.creating SOA RR");
         soaResourceRecord = new ResourceRecord(DomainName.ofString(handleDNSZone),ResourceRecord.TYPE_SOA,ResourceRecord.CLASS_IN,8600,soaDataAsString.toString());
        }
        catch(ParseException pe){
        	pe.printStackTrace();
        }
    	return soaResourceRecord;
    	
    }
    
    
    private List<ResourceRecord> getNSforGHR(String handleDNSZone,StreamTable config){
    	
    	ArrayList<ResourceRecord> nsRecords = new ArrayList<ResourceRecord>();
    	Vector nsVec = (Vector)config.get("ns_record");
    	
    	try{
    		for(int i=0; i<nsVec.size(); i++) {
    			String ns = String.valueOf(nsVec.elementAt(i));
    			ResourceRecord nsRecord = new ResourceRecord(DomainName.ofString(handleDNSZone),ResourceRecord.TYPE_NS,ResourceRecord.CLASS_IN,8600,ns);
    		}
    	}
    	 catch(ParseException pe){
         	pe.printStackTrace();
         }
        
    	return nsRecords;
    	
    }
    
    
    public boolean isCertify() {
        return certify;
    }
    
    private class InetAddressRange {
        byte[] address;
        int prefix;
        
        InetAddressRange(String s) throws UnknownHostException {
            int n = s.indexOf('/');
            if(n>=0) {
                String s1 = s.substring(0,n).trim();
                address = InetAddress.getByName(s1).getAddress();
                prefix = Integer.parseInt(s.substring(n+1).trim());
            }
            else {
                String s1 = s.trim();
                address = InetAddress.getByName(s1).getAddress();
                prefix = 8 * address.length;                
            }
        }
        
        boolean matches(InetAddress addr) {
            byte[] addrBytes = addr.getAddress();
            if(addrBytes.length!=address.length) return false;
            int prefix = this.prefix;
            for(int i = 0; i < address.length; i++) {
                if(prefix<=0) return true;
                int mask = 0xFF;
                if(prefix < 8) mask = mask << (8-prefix);
                if((addrBytes[i] & mask) != (address[i] & mask)) return false;
                prefix -= 8;
            }
            return true;
        }
    }
    
    /**
     * Returns whether or not to fall back on DNS
     * when unable to resolve via handles
     */
    public boolean getRecursive(InetAddress addr){
        if(allow_recursion==null || addr==null) return recursive;
        byte[] addrBytes = addr.getAddress();
        for(int i = 0; i < allow_recursion.length; i++) {
            if(allow_recursion[i].matches(addr)) return true;
        }
        return false;
    }

    /**
     * Returns whether or not to fall back on DNS
     * when unable to resolve via handles
     */
    public boolean getAllowQuery(InetAddress addr){
        if(allow_query==null) return true;
        if(addr==null) return false;
        for(int i = 0; i < allow_query.length; i++) {
            if(allow_query[i].matches(addr)) return true;
        }
        return false;
    }

    private List<Storage> _storageList;
    public List<Storage> storageList() {
        return _storageList;
    }
    
    private NameServer _nameServer;
    public NameServer getNameServer() {
        return _nameServer;
    }
    
    private NameResolver _resolver;
    public NameResolver getNameResolver() {
        return _resolver;
    }
    
    /**
     * Returns the dns handle prefix
     */
    public String getDnsHandlePrefix(String suffix){
        if(dnsHandleMap==null) return null;
        return (String)dnsHandleMap.get(suffix.toLowerCase());
    }   
    
	public synchronized void writeToFile(String s) {

	FileWriter output = null;
  	try {
  		output = new FileWriter("/tmp/hslog",true);
  		BufferedWriter writer = new BufferedWriter(output);
  		
  		
  		writer.write(s+"\n");
  		writer.close();

  	} catch (Exception e) {
  	System.out.println("filewritererror");
    throw new RuntimeException(e);
  } finally {
    if (output != null) {
      try {
        output.close();
      } catch (IOException e) {
        // Ignore issues during closing
      }
    }
  }
}
}
