/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.

        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
          http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.handle.dnslib;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.lang.reflect.Field;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.handle.hdllib.*;
import net.handle.server.AbstractServer;
import net.handle.server.dns.DnsConfiguration;

public class Storage {
    
	private DomainName handleZone;
    private DomainName zone;
    public String handlePrefix;
    private AbstractServer server;
    private DnsConfiguration dnsConfig;
    public ResourceRecord soaResourceRecord;
    public List<ResourceRecord> nsResourceRecords; 
    
    public Storage(DomainName zone, String handlePrefix, AbstractServer server, DnsConfiguration dnsConfig) {
    	 //writeToFile("Storage()zone = "+zone.toString());
        this.zone = zone;
        this.handlePrefix = handlePrefix;
        this.server = server;
        this.dnsConfig = dnsConfig;
        
    }
    
    public void setHandleZone(DomainName zone){
    	this.handleZone = zone;
    }
    
 
    
    private static Map<Integer,byte[]> dnsTypeToHandleTypeMap = new HashMap<Integer,byte[]>();
    private static Map<String,Integer> handleTypeToDNSTypeMap = new HashMap<String,Integer>();
    static {
        Field[] allFields = ResourceRecord.class.getFields();
        for(Field field : allFields) {
            String name = field.getName();
            try {
                if(name.startsWith("TYPE_")) {
                    int type = field.getInt(null);
                    dnsTypeToHandleTypeMap.put(type,Util.encodeString("DNS." + name.substring(5)));
                    handleTypeToDNSTypeMap.put("DNS." + name.substring(5),type);
                }
            }
            catch(Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    private static final byte[] SYMBOLIC_CNAME = Util.encodeString("DNS.CNAME");
    private static final byte[] NUMERIC_CNAME = Util.encodeString("DNS.TYPE5");
    
    private static final byte[][] HDL_DNS_TYPES_ANY = new byte[][] { Util.encodeString("DNS.") };
    private static final byte[][] HDL_DNS_TYPES_CNAME = new byte[][] { SYMBOLIC_CNAME, NUMERIC_CNAME };     

    private static byte[] handleTypeSymbolic(int type) {
        return dnsTypeToHandleTypeMap.get(type);
    }
    private static byte[] handleTypeNumeric(int type) {
        return Util.encodeString("DNS.TYPE" + type);
    }
    
    public static byte[][] dnsTypeToHandleTypes(int type, boolean includeCNAME) {
        if(type==Message.QTYPE_ANY) return HDL_DNS_TYPES_ANY;
        if(type==ResourceRecord.TYPE_CNAME) return HDL_DNS_TYPES_CNAME;
        
        if(type==Message.QTYPE_MAILA) {
            if(includeCNAME) return new byte[][] { handleTypeSymbolic(ResourceRecord.TYPE_MD), handleTypeNumeric(ResourceRecord.TYPE_MD),
                    handleTypeSymbolic(ResourceRecord.TYPE_MF), handleTypeNumeric(ResourceRecord.TYPE_MF),
                    SYMBOLIC_CNAME, NUMERIC_CNAME };
            else return new byte[][] { handleTypeSymbolic(ResourceRecord.TYPE_MD), handleTypeNumeric(ResourceRecord.TYPE_MD),
                    handleTypeSymbolic(ResourceRecord.TYPE_MF), handleTypeNumeric(ResourceRecord.TYPE_MF) };
        }
        else if(type==Message.QTYPE_MAILB) {
            if(includeCNAME) return new byte[][] { handleTypeSymbolic(ResourceRecord.TYPE_MB), handleTypeNumeric(ResourceRecord.TYPE_MB),
                    handleTypeSymbolic(ResourceRecord.TYPE_MG), handleTypeNumeric(ResourceRecord.TYPE_MG),
                    handleTypeSymbolic(ResourceRecord.TYPE_MR), handleTypeNumeric(ResourceRecord.TYPE_MR),
                    SYMBOLIC_CNAME, NUMERIC_CNAME };
            else return new byte[][] { handleTypeSymbolic(ResourceRecord.TYPE_MB), handleTypeNumeric(ResourceRecord.TYPE_MB),
                    handleTypeSymbolic(ResourceRecord.TYPE_MG), handleTypeNumeric(ResourceRecord.TYPE_MG),
                    handleTypeSymbolic(ResourceRecord.TYPE_MR), handleTypeNumeric(ResourceRecord.TYPE_MR) };
        }

        byte[] symbolic = handleTypeSymbolic(type);
        if(symbolic==null) {
            if(includeCNAME) return new byte[][] { handleTypeNumeric(type), SYMBOLIC_CNAME, NUMERIC_CNAME };
            else return new byte[][] { handleTypeNumeric(type) };
        }
        else {
            if(includeCNAME) return new byte[][] { symbolic, handleTypeNumeric(type), SYMBOLIC_CNAME, NUMERIC_CNAME };
            else return new byte[][] { symbolic, handleTypeNumeric(type) };
        }
    }
    
   
    
    public static ResourceRecord handleValueToResourceRecordMapping(DomainName name, HandleValue value) {
        String handleTypeString = value.getTypeAsString();
        Integer type = ResourceRecord.TYPE_TXT;
        String data = handleTypeString+"="+value.getDataAsString();
        int ttl = value.getTTL();
        if(value.getTTLType()==HandleValue.TTL_TYPE_ABSOLUTE) {
            ttl = ttl - (int)(System.currentTimeMillis()/1000);
        }
        if(ttl<0) ttl = 0;
        try {
            return new ResourceRecord(name,type,ResourceRecord.CLASS_IN,ttl,data);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public static ResourceRecord handleValueToResourceRecord(DomainName name, HandleValue value) {
        String typeString = value.getTypeAsString();
        Integer type = handleTypeToDNSTypeMap.get(typeString);
        if(type==null) {
            try {
                if(typeString.startsWith("DNS.TYPE")) type = Integer.parseInt(typeString.substring(8));
                else return null;
            }
            catch(NumberFormatException e) { return null; }
        }
        int ttl = value.getTTL();
        if(value.getTTLType()==HandleValue.TTL_TYPE_ABSOLUTE) {
            ttl = ttl - (int)(System.currentTimeMillis()/1000);
        }
        if(ttl<0) ttl = 0;
        try {
            return new ResourceRecord(name,type,ResourceRecord.CLASS_IN,ttl,value.getDataAsString());
        }
        catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public DomainName getHandleZone() {
        return handleZone;
    }
    
    public DomainName getZone() {
        return zone;
    }
    
    /** This provides a simple way to call into the server's processRequest and use the response */
    private class SimpleResponseMessageCallback implements ResponseMessageCallback {
        public AbstractResponse response = null;
        public void handleResponse(AbstractResponse response) /*throws HandleException*/ {
            this.response = response;
        }
    }
    
    

    
    List<ResourceRecord> getHandleRecord(DomainName name,byte [] handle, int type, boolean includeCNAME) throws HandleException {
    	
  //  	writeToFile("Storage.getHandleRecord of ");
    /*
    	StackTraceElement stackTrace [] = Thread.currentThread().getStackTrace();
    	for (int i=0;i<stackTrace.length;i++){
    		writeToFile("Storage.getHandleRecord-StackTrace="+stackTrace[i].toString());
    	}
    	*/
        
        /*  we dont not that
        byte[][] handleTypes = dnsTypeToHandleTypes(type,includeCNAME);
        */
        
        // get all HandleValues
    //	writeToFile("Storage.getHandleRecord-bfore-ResolutionRequest");
        ResolutionRequest req = new ResolutionRequest(handle,null,null,null);
        
        if(dnsConfig.isCertify()) req.certify = true;
        SimpleResponseMessageCallback callback = new SimpleResponseMessageCallback();
        server.processRequest(req,callback);
        if(callback.response instanceof ResolutionResponse) {
            HandleValue[] values = ((ResolutionResponse)callback.response).getHandleValues();
            ArrayList<ResourceRecord> res = new ArrayList<ResourceRecord>();
            for(HandleValue value : values) {
            	/* map HandleValue to a DNS TXT RR with data-field = "HandleValue.type=HandleValue.data" */
                ResourceRecord rr = handleValueToResourceRecordMapping(name,value);
                if(rr!=null) res.add(rr);
            }
            return res;
        }
        else {
            if(callback.response==null) throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected null response");
            if(callback.response.responseCode==AbstractResponse.RC_HANDLE_NOT_FOUND) return null;
            if(callback.response.responseCode==AbstractResponse.RC_VALUES_NOT_FOUND) return Collections.emptyList();
            throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected response: " + callback.response);
        }
    }
    
    List<ResourceRecord> getRecords(DomainName name, int type, boolean includeCNAME) throws HandleException {
    	/*
    	StackTraceElement stackTrace [] = Thread.currentThread().getStackTrace();
    	for (int i=0;i<stackTrace.length;i++){
    		writeToFile("Storage.getRecords-StackTrace="+stackTrace[i].toString());
    	}
    	*/
        byte[] handle;
        //writeToFile("Storage.getRecords-handlePrefix="+handlePrefix);
        //writeToFile("Storage.getRecords-DomainName="+name.toString());
        try {
            handle = name.toHandle(handlePrefix,zone.length());
          //  writeToFile("Storage.getRecords-handle="+Util.decodeString(handle));
        }
        catch(ParseException e) {
            e.printStackTrace();
            return null;
        }
        
        /*  we dont not that
        byte[][] handleTypes = dnsTypeToHandleTypes(type,includeCNAME);
        */
        
        // get all HandleValues
        
        ResolutionRequest req = new ResolutionRequest(handle,null,null,null);
        
        if(dnsConfig.isCertify()) req.certify = true;
        SimpleResponseMessageCallback callback = new SimpleResponseMessageCallback();
        server.processRequest(req,callback);
        if(callback.response instanceof ResolutionResponse) {
            HandleValue[] values = ((ResolutionResponse)callback.response).getHandleValues();
            ArrayList<ResourceRecord> res = new ArrayList<ResourceRecord>();
            for(HandleValue value : values) {
            	/* map HandleValue to a DNS TXT RR with data-field = "HandleValue.type=HandleValue.data" */
                ResourceRecord rr = handleValueToResourceRecordMapping(name,value);
                if(rr!=null) res.add(rr);
            }
            return res;
        }
        else {
            if(callback.response==null) throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected null response");
            if(callback.response.responseCode==AbstractResponse.RC_HANDLE_NOT_FOUND) return null;
            if(callback.response.responseCode==AbstractResponse.RC_VALUES_NOT_FOUND) return Collections.emptyList();
            throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected response: " + callback.response);
        }
    }
    
    
    public ResourceRecord resolveSOARecord(String soaRecordHandle,DomainName name, boolean includeCNAME) throws HandleException {
    	
    	/*
    	StackTraceElement stackTrace [] = Thread.currentThread().getStackTrace();
    	for (int i=0;i<stackTrace.length;i++){
    		writeToFile("Storage.getRecords-StackTrace="+stackTrace[i].toString());
    	}
    	*/
    	byte[][] handleTypes = dnsTypeToHandleTypes(ResourceRecord.TYPE_SOA,includeCNAME);
        ResolutionRequest req = new ResolutionRequest(Util.encodeString(soaRecordHandle),handleTypes,null,null);
        
   
        if(dnsConfig.isCertify()) req.certify = true;
        SimpleResponseMessageCallback callback = new SimpleResponseMessageCallback();
        server.processRequest(req,callback);
        if(callback.response instanceof ResolutionResponse) {
            HandleValue[] values = ((ResolutionResponse)callback.response).getHandleValues();
            ArrayList<ResourceRecord> res = new ArrayList<ResourceRecord>();
            for(HandleValue value : values) {
                ResourceRecord rr = handleValueToResourceRecord(zone,value);
                if(rr!=null) res.add(rr);
            }
            
            return res.get(0);
        }
        else {
            if(callback.response==null) throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected null response");
            if(callback.response.responseCode==AbstractResponse.RC_HANDLE_NOT_FOUND) return null;
            if(callback.response.responseCode==AbstractResponse.RC_VALUES_NOT_FOUND) return null;
            throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected response: " + callback.response);
        }
    }
    
    public List<ResourceRecord> resolveNSRecords(String nsRecordHandle,DomainName name, boolean includeCNAME) throws HandleException {
    	/*
    	StackTraceElement stackTrace [] = Thread.currentThread().getStackTrace();
    	for (int i=0;i<stackTrace.length;i++){
    		writeToFile("Storage.getRecords-StackTrace="+stackTrace[i].toString());
    	}
    	*/
    	byte[][] handleTypes = dnsTypeToHandleTypes(ResourceRecord.TYPE_NS,includeCNAME);
        ResolutionRequest req = new ResolutionRequest(Util.encodeString(nsRecordHandle),handleTypes,null,null);
        
   
        if(dnsConfig.isCertify()) req.certify = true;
        SimpleResponseMessageCallback callback = new SimpleResponseMessageCallback();
        server.processRequest(req,callback);
        if(callback.response instanceof ResolutionResponse) {
            HandleValue[] values = ((ResolutionResponse)callback.response).getHandleValues();
            ArrayList<ResourceRecord> res = new ArrayList<ResourceRecord>();
            for(HandleValue value : values) {
                ResourceRecord rr = handleValueToResourceRecord(zone,value);
                if(rr!=null) res.add(rr);
            }
            return res;
        }
        else {
            if(callback.response==null) throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected null response");
            if(callback.response.responseCode==AbstractResponse.RC_HANDLE_NOT_FOUND) return null;
            if(callback.response.responseCode==AbstractResponse.RC_VALUES_NOT_FOUND) return Collections.emptyList();
            throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected response: " + callback.response);
        }
    }
    
    
    public List<ResourceRecord> getNSRecordsFromSiteInfo(String zeroNaHandle, DomainName name)  throws HandleException{
    	
    	//writeToFile("Storage.getNSRecordsFromSiteInfo.get the HS_SITE Records for"+zeroNaHandle);
    	ResolutionRequest req = new ResolutionRequest(Util.encodeString(zeroNaHandle),null,null,null);
    	
    	if(dnsConfig.isCertify()) req.certify = true;
        SimpleResponseMessageCallback callback = new SimpleResponseMessageCallback();
        //writeToFile("Storage.getNSRecordsFromSiteInfo. doing processRequest");
        server.processRequest(req,callback);
        //writeToFile("Storage.getNSRecordsFromSiteInfo. done with processRequest");
        if(callback.response instanceof ResolutionResponse) {
            HandleValue[] values = ((ResolutionResponse)callback.response).getHandleValues();
            ArrayList<ResourceRecord> res = new ArrayList<ResourceRecord>();
            int ttl = values[0].getTTL();
            if(values[0].getTTLType()==HandleValue.TTL_TYPE_ABSOLUTE) {
                ttl = ttl - (int)(System.currentTimeMillis()/1000);
            }
            if(ttl<0) ttl = 0;
           // writeToFile("Storage.getNSRecordsFromSiteInfo. getting the SitesFromValues");
            SiteInfo [] siteInfo  = Util.getSitesFromValues(values);
            for(int i=0;i<siteInfo.length;i++){
            //	 writeToFile("Storage.getNSRecordsFromSiteInfo.looping through SiteInfos...");
            	ServerInfo [] servers = siteInfo[i].servers;
            	for(int j=0;j<servers.length;j++){
            		// writeToFile("Storage.getNSRecordsFromSiteInfo.looping through ServerInfos...");
            		 try{
            		 String serverAddress = servers[j].getAddressString();
            		InetAddress addr =  InetAddress.getByName(serverAddress);
            		String host = addr.getCanonicalHostName();
            		
            		
            		//	 writeToFile("Storage.getNSRecordsFromSiteInfo.looping through ServerInfoANDCreating-NS-RRfor IP="+host);
            			// writeToFile("Storage.getNSRecordsFromSiteInfo.looping.TTL="+String.valueOf(ttl));
            		ResourceRecord rr = new ResourceRecord(name,ResourceRecord.TYPE_NS,ResourceRecord.CLASS_IN,ttl,host);
            		res.add(rr);
            	 }
                catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            		catch(UnknownHostException ue){
            			ue.printStackTrace();
            		}
            	}
            }
            return res;
            
        }
        else {
        	// writeToFile("Storage.getNSRecordsFromSiteInfo error in Resolution");
            if(callback.response==null) throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected null response");
            if(callback.response.responseCode==AbstractResponse.RC_HANDLE_NOT_FOUND) return null;
            if(callback.response.responseCode==AbstractResponse.RC_VALUES_NOT_FOUND) return Collections.emptyList();
            throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected response: " + callback.response);
        }
    	
    	
    	
    	
    }
    
    
    List<ResourceRecord> getPureDNSRecords(DomainName name, int type, boolean includeCNAME) throws HandleException {
    	/*
    	StackTraceElement stackTrace [] = Thread.currentThread().getStackTrace();
    	for (int i=0;i<stackTrace.length;i++){
    		writeToFile("Storage.getRecords-StackTrace="+stackTrace[i].toString());
    	}
    	*/
        byte[] handle;
      //  writeToFile("Storage.getRecords-handlePrefix="+handlePrefix);
        //writeToFile("Storage.getRecords-DomainName="+name.toString());
        try {
            handle = name.toHandle(handlePrefix,zone.length());
            //writeToFile("Storage.getRecords-handle="+Util.decodeString(handle));
        }
        catch(ParseException e) {
            e.printStackTrace();
            return null;
        }
        byte[][] handleTypes = dnsTypeToHandleTypes(type,includeCNAME);
        
        ResolutionRequest req = new ResolutionRequest(handle,handleTypes,null,null);
        
   
        if(dnsConfig.isCertify()) req.certify = true;
        SimpleResponseMessageCallback callback = new SimpleResponseMessageCallback();
        server.processRequest(req,callback);
        if(callback.response instanceof ResolutionResponse) {
            HandleValue[] values = ((ResolutionResponse)callback.response).getHandleValues();
            ArrayList<ResourceRecord> res = new ArrayList<ResourceRecord>();
            for(HandleValue value : values) {
                ResourceRecord rr = handleValueToResourceRecord(name,value);
                if(rr!=null) res.add(rr);
            }
            return res;
        }
        else {
            if(callback.response==null) throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected null response");
            if(callback.response.responseCode==AbstractResponse.RC_HANDLE_NOT_FOUND) return null;
            if(callback.response.responseCode==AbstractResponse.RC_VALUES_NOT_FOUND) return Collections.emptyList();
            throw new HandleException(HandleException.INTERNAL_ERROR,"Unexpected response: " + callback.response);
        }
    }
    
    List<ResourceRecord> getWildcardRecords(DomainName wildcard, DomainName name, int type, boolean includeCNAME) throws HandleException {
        List<ResourceRecord> rrs = getRecords(wildcard,type,includeCNAME);
        if(rrs==null) return null;
        ArrayList<ResourceRecord> newRRs = new ArrayList<ResourceRecord>(rrs.size());
        for(ResourceRecord rr : rrs) {
            ResourceRecord newRR = new ResourceRecord(name,rr.getTTL(),rr);
            newRRs.add(rr);
        }
        return newRRs;
    }
    
   
    List<ResourceRecord> getHandleRecord(DomainName name,byte [] handle, int type) throws HandleException {
        return getHandleRecord(name,handle,type,true);
    }
    
    List<ResourceRecord> getRecords(DomainName name, int type) throws HandleException {
        return getRecords(name,type,true);
    }
    
    List<ResourceRecord> getNSRecords(DomainName name) throws HandleException {
    	
    	if (name.toString().equals(this.zone.toString())){
    		//writeToFile("storage.getNSRecords:returning nsRecordsFromHandle");
    		return this.nsResourceRecords;
    	}
    	else{
    		return getRecords(name,ResourceRecord.TYPE_NS,false); 
    	}
    	
    }
    
    List<ResourceRecord> getWildcardNSRecords(DomainName wildcard, DomainName name) throws HandleException {
        return getWildcardRecords(wildcard,name,ResourceRecord.TYPE_NS,false);
    }

    List<ResourceRecord> getWildcardRecords(DomainName wildcard, DomainName name, int type) throws HandleException {
        return getWildcardRecords(wildcard,name,type,true);
    }

    ResourceRecord getSOARecord(DomainName name) throws HandleException {
    	
    	
    	if (name.toString().equals(this.zone.toString())){
    		return this.soaResourceRecord;
    	}
    	else{
    		List<ResourceRecord> rrs = getRecords(name,ResourceRecord.TYPE_SOA,false);
            if(rrs==null || rrs.size()!=1) return null;
            return rrs.get(0);
            
    	}
        /*List<ResourceRecord> rrs = getRecords(name,ResourceRecord.TYPE_SOA,false);
        if(rrs==null || rrs.size()!=1) return null;
        return rrs.get(0);
        */
    	
        
    }
    
    
	public synchronized void writeToFile(String s) {

	FileWriter output = null;
  	try {
  		output = new FileWriter("/tmp/hslog",true);
  		BufferedWriter writer = new BufferedWriter(output);
  		
  		
  		writer.write(s+"\n");
  		writer.close();

  	} catch (Exception e) {
  	System.out.println("filewritererror");
    throw new RuntimeException(e);
  } finally {
    if (output != null) {
      try {
        output.close();
      } catch (IOException e) {
        // Ignore issues during closing
      }
    }
  }
}
    
}
